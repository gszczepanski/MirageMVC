/**
 * 
 */
package mirage.mvc.model;

/**
 * 
 * @author Gerard Szczepanski
 */
public interface Model {
	
	Model addAttribute(String key, Object value);
	
	Model addAttributes(String key, Object[] values);
	
	
	
}
