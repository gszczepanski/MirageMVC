/**
 * 
 */
package mirage.mvc.model;

/**
 * @author Gerard Szczepanski
 *
 */
public class MirageModelProxy implements Model{
	
	private final CoreModel model;
	
	public MirageModelProxy(CoreModel model) {
		this.model = model;
	}

	@Override
	public Model addAttribute(String key, Object value) {
		return model.addAttribute(key, value);
	}

	@Override
	public Model addAttributes(String key, Object[] values) {
		return model.addAttributes(key, values);
	}

}
