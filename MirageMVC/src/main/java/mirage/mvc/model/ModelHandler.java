/**
 * 
 */
package mirage.mvc.model;

/**
 * @author Gerard Szczepanski
 *
 */
public interface ModelHandler {
	
	void registerModel(int requestId);
	
	Model getModelProxy(int requestId);
	
	CoreModel getModel(int requestId);
	
	void destroyModel(int requestId);
	
}
