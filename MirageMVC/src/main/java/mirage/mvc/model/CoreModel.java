/**
 * 
 */
package mirage.mvc.model;

/**
 * @author Gerard Szczepanski
 *
 */
public interface CoreModel extends Model{
	
	Object getAttribute(String key);
	
}
