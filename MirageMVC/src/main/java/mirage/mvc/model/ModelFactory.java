/**
 * 
 */
package mirage.mvc.model;

/**
 * @author Gerard Szczepanski
 *
 */
public class ModelFactory {
	
	private ModelFactory(){
		throw new IllegalAccessError("Dont initialize this class!");
	}
	
	public static CoreModel constructCoreModel(int requestId){
		return new MirageModel(requestId);
	}
	
	public static Model constructMirageModelProxy(CoreModel model){
		return new MirageModelProxy(model);
	}
	
}
