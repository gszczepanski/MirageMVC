/**
 * 
 */
package mirage.mvc.model;

import java.util.HashMap;
import java.util.Map;

import mirage.mvc.exceptions.MirageMvcRuntimeError;

/**
 * This class manages Model objects across the MirageMVC.
 * 
 * @author Gerard Szczepanski
 *
 */
public class MirageModelHandler implements ModelHandler {
	private static MirageModelHandler singleton;

	private final Map<Integer, CoreModel> modelContainer;

	private MirageModelHandler() {
		modelContainer = new HashMap<>();
	}

	public static MirageModelHandler getModelHandler() {
		if (singleton == null) {
			singleton = new MirageModelHandler();
		}

		return singleton;
	}

	@Override
	public void registerModel(int requestId) {
		ifModelExistThrowError(requestId);
		modelContainer.put(requestId, ModelFactory.constructCoreModel(requestId));
	}

	private void ifModelExistThrowError(int requestId) {
		if (modelContainer.get(requestId) != null) {
			throw new MirageMvcRuntimeError(
					String.format("Registering Model failed. Model of given id [%s] already exist!", requestId));
		}
	}

	@Override
	public Model getModelProxy(int requestId) {
		return ModelFactory.constructMirageModelProxy(getModel(requestId));
	}

	@Override
	public CoreModel getModel(int requestId) {
		CoreModel model = modelContainer.get(requestId);
		if (model != null) {
			return model;
		} else {
			throw new MirageMvcRuntimeError(String.format("Model with id [%s] doesn't exists in ModelHandler", requestId));
		}
	}

	@Override
	public void destroyModel(int requestId) {
		modelContainer.put(requestId, null);
	}

}
