package mirage.mvc.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import mirage.mvc.request.HttpMirageRequest;

public class DispatcherServlet extends CoreServlet{
	private static final long serialVersionUID = 5946190665026025091L;
	
	PrintWriter out;

	@Override
	protected void handleRequestAndResponse(HttpMirageRequest req, HttpServletResponse resp) throws IOException {
		out = resp.getWriter();
		out.println("<h1><i>Welcome to MirageMVC! Process...</i></h1>");
	}
	
}
