package mirage.mvc.servlet;

import static mirage.mvc.request.RequestType.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mirage.mvc.request.HttpMirageRequest;

public abstract class CoreServlet extends HttpServlet{
	private static final long serialVersionUID = -6267791215442867635L;

	@Override
	public void init() throws ServletException {
		log("INITIALIZING");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequestAndResponse(new HttpMirageRequest(req, GET), resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequestAndResponse(new HttpMirageRequest(req, POST), resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequestAndResponse(new HttpMirageRequest(req, PUT), resp);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequestAndResponse(new HttpMirageRequest(req, DELETE), resp);
	}
	
	protected abstract void handleRequestAndResponse(HttpMirageRequest req, HttpServletResponse resp) throws IOException;
}
