package mirage.mvc.request;

/**
 * Defines type of the HttpRequest.
 * @author Gerard Szczepanski
 */
public enum RequestType {
	GET,
	POST,
	PUT,
	DELETE;
}
