package mirage.mvc.request;

import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import lombok.Getter;

/**
 * Encapsulates HttpServletRequest and its RequestType. Request type is defined
 * when HttpServlet income to DispatherServlet.
 * 
 * @author Gerard Szczepanski
 */
@Getter
public class HttpMirageRequest {
	private static final AtomicInteger idGenerator = new AtomicInteger(0);
	
	private final int id;
	private final HttpServletRequest request;
	private final RequestType type;

	public HttpMirageRequest(HttpServletRequest request, RequestType type) {
		this.request = request;
		this.type = type;
		this.id = idGenerator.incrementAndGet();
	}

}
