package mirage.mvc.core.handlers;

import javax.servlet.http.HttpServletResponse;

import mirage.mvc.request.HttpMirageRequest;

public interface RequestHandler {
	
	void handleRequest(HttpMirageRequest req, HttpServletResponse resp);
	
}
