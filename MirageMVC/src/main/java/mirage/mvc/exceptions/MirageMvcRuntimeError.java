/**
 * 
 */
package mirage.mvc.exceptions;

/**
 * @author Gerard Szczepanski
 *
 */
public class MirageMvcRuntimeError extends Error{
	private static final long serialVersionUID = -203987134505509152L;

	public MirageMvcRuntimeError() {
		super();
	}

	public MirageMvcRuntimeError(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MirageMvcRuntimeError(String message, Throwable cause) {
		super(message, cause);
	}

	public MirageMvcRuntimeError(String message) {
		super(message);
	}

	public MirageMvcRuntimeError(Throwable cause) {
		super(cause);
	}
	
}
