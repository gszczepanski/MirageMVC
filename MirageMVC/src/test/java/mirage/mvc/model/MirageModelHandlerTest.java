/**
 * 
 */
package mirage.mvc.model;

import org.testng.Assert;
import org.testng.annotations.Test;

import mirage.mvc.exceptions.MirageMvcRuntimeError;

/**
 * @author Gerard Szczepanski
 *
 */
public class MirageModelHandlerTest {

	@Test
	public void getModelProxy() {
		// Arrange
		MirageModelHandler handler = MirageModelHandler.getModelHandler();
		int requestId = 1;

		// Act
		handler.registerModel(requestId);
		Model model = handler.getModelProxy(requestId);

		// Assert
		Assert.assertNotNull(model);

	}
	
	@Test(expectedExceptions = MirageMvcRuntimeError.class)
	public void getModelProxyFailure() {
		// Arrange
		MirageModelHandler handler = MirageModelHandler.getModelHandler();
		int requestId = 50000000;

		Model model = handler.getModelProxy(requestId);
	}

	@Test
	public void getModel() {
		// Arrange
		MirageModelHandler handler = MirageModelHandler.getModelHandler();
		int requestId = 2;

		// Act
		handler.registerModel(requestId);
		CoreModel model = handler.getModel(requestId);

		// Assert
		Assert.assertNotNull(model);
	}
	
	@Test(expectedExceptions = MirageMvcRuntimeError.class)
	public void getModelFailure() {
		// Arrange
		MirageModelHandler handler = MirageModelHandler.getModelHandler();
		int requestId = 1000000;

		// Act
		CoreModel model = handler.getModel(requestId);
	}

	@Test(expectedExceptions = MirageMvcRuntimeError.class)
	public void registerModelFailure() {
		// Arrange
		MirageModelHandler handler = MirageModelHandler.getModelHandler();
		int requestId = 999;

		// Act
		handler.registerModel(requestId);
		handler.registerModel(requestId);
	}

	@Test
	public void destroyModel() {
		// Arrange
		MirageModelHandler handler = MirageModelHandler.getModelHandler();
		int requestId = 1234;
		
		//Act
		handler.registerModel(requestId);
		handler.destroyModel(requestId);
		handler.registerModel(requestId);
		
		CoreModel model = handler.getModel(requestId);
		
		//Assert
		Assert.assertNotNull(model);
	}

}
